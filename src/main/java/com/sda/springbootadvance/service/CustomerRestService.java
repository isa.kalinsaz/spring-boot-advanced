package com.sda.springbootadvance.service;

import com.sda.springbootadvance.model.CustomerDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpMethod.GET;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerRestService {

    @Value("${external.customer.api}")
    private String externalCustomerApi;

    private final RestTemplate restTemplate;

    public List<CustomerDto> getCustomers() {
        final ResponseEntity<List<CustomerDto>> responseEntity = restTemplate.exchange(externalCustomerApi + "/api/service/customers", GET, null, new ParameterizedTypeReference<List<CustomerDto>>() {
        });
        return responseEntity.getBody();
    }

    public CustomerDto getCustomer(final Long customerId) {
        final Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", customerId);
        final ResponseEntity<CustomerDto> response = restTemplate.getForEntity(externalCustomerApi + "/api/service/customers/{id}", CustomerDto.class, uriVariables);
        if (response.getBody() != null) {
            return response.getBody();
        } else {
            log.error("No customer found with customerId {}", customerId);
            throw new RuntimeException("No customer found with customerId : " + customerId);
        }
    }

}
