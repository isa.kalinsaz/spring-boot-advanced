package com.sda.springbootadvance.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Queue;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerJmsService {

    private final Queue queue;
    private final JmsTemplate jmsTemplate;

    public void sendJmsMessage(final String message) {

        jmsTemplate.send(queue, s -> s.createTextMessage(message));

        log.info("The JMS Message '{}' has published!!! ", message);
    }

}
