package com.sda.springbootadvance.model;

public enum UserType {
    PRIVATE,
    BUSINESS
}
