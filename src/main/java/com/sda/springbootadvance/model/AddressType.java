package com.sda.springbootadvance.model;

public enum AddressType {
    HOME,
    OFFICE
}
