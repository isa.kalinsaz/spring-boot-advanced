package com.sda.springbootadvance.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDto {

    private Long id;
    private UserType userType;
    private String name;
    private String surname;
    private Integer age;
    private BigDecimal monthlyIncome;
    private String emailAddress;
    private List<AddressDto> addressDtos;

}
