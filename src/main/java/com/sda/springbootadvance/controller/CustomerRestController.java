package com.sda.springbootadvance.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sda.springbootadvance.model.CustomerDto;
import com.sda.springbootadvance.service.CustomerJmsService;
import com.sda.springbootadvance.service.CustomerRestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/customers")
public class CustomerRestController {

    private final ObjectMapper objectMapper;
    private final CustomerJmsService customerJmsService;
    private final CustomerRestService customerRestService;

    @GetMapping
    public List<CustomerDto> getCustomers() {
        return customerRestService.getCustomers();
    }

    @GetMapping("/{id}")
    public CustomerDto getCustomer(@PathVariable("id") final Long customerId) {
        return customerRestService.getCustomer(customerId);
    }

    @GetMapping("message/{customerId}")
    public ResponseEntity sendMessageToCustomer(@PathVariable("customerId") final Long customerId) throws JsonProcessingException {

        final CustomerDto customerDto = customerRestService.getCustomer(customerId);
        final String message = objectMapper.writeValueAsString(customerDto);
        customerJmsService.sendJmsMessage(message);
        return ResponseEntity.ok().build();
    }

}
